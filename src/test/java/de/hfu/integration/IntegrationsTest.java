package de.hfu.integration;

import de.hfu.integration.domain.Resident;
import de.hfu.integration.repository.ResidentRepository;
import de.hfu.integration.service.BaseResidentService;
import de.hfu.integration.service.ResidentService;
import de.hfu.integration.service.ResidentServiceException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.easymock.EasyMock.*;
import static org.junit.jupiter.api.Assertions.*;

public class IntegrationsTest {

    ResidentRepositoryStub stub = mock(ResidentRepositoryStub.class);

    @BeforeEach
    public void init() {
        expect(stub.getResidents()).andReturn(getResidentsMock());
        replay(stub);
    }

                // Filtered Tests
    @Test
    public void getFilteredResidentTestSuccess() {
        BaseResidentService service = new BaseResidentService();
        service.setResidentRepository(stub);
        List<Resident> residents = service.getFilteredResidentsList(new Resident("Vadim", "" , "", "", null));
        if(residents.size() != 1) fail();
        assertEquals(residents.get(0).getGivenName(), "Vadim");
    }

    @Test
    public void getFilteredResidentTestSuccessWildcard() {
        BaseResidentService service = new BaseResidentService();
        service.setResidentRepository(stub);
        List<Resident> residents = service.getFilteredResidentsList(new Resident("V*", "" , "", "", null));
        assertEquals(residents.size(), 2);
    }

    @Test
    public void getFilteredResidentTestNoResults() {
        BaseResidentService service = new BaseResidentService();
        service.setResidentRepository(stub);
        List<Resident> residents = service.getFilteredResidentsList(new Resident("Herbert", "" , "", "", new Date()));
        assertEquals(residents.size(), 0);
    }


                //Unique Tests
    @Test
    public void getUniqueResidentTestSuccess() throws ResidentServiceException {
        BaseResidentService service = new BaseResidentService();
        service.setResidentRepository(stub);
        Resident resident = service.getUniqueResident(new Resident("Vadim", "" , "", "", null));
        if(resident == null) fail();
        assertEquals(resident.getGivenName(), "Vadim");
    }

    @Test
    public void getUniqueResidentTestFail() {
        BaseResidentService service = new BaseResidentService();
        service.setResidentRepository(stub);
        assertThrows(ResidentServiceException.class, () -> {
            service.getUniqueResident(new Resident("Vadim1", "" , "", "", null));
        });
    }

    @Test
    public void getUniqueResidentTestFailWildcard() {
        BaseResidentService service = new BaseResidentService();
        service.setResidentRepository(stub);
        assertThrows(ResidentServiceException.class, () -> {
            service.getUniqueResident(new Resident("Va*", "" , "", "", null));
        });
    }

    private List<Resident> getResidentsMock() {
        Resident resident1 = new Resident("Vadim", "Felker", "sdasdd 7", "Furtwangen", new Date(2000,02,23) );
        Resident resident2 = new Resident("Serghei", "sadads", "sdasdd 7", "Furtwangen", new Date(2000,05,8) );
        Resident resident3 = new Resident("Enis", "sadf", "sdasdd 7", "Furtwangen", new Date(2001,11,18) );
        Resident resident4 = new Resident("Yannick", "Weber", "sdasdd 7", "Furtwangen", new Date(2003,01,27) );
        Resident resident5 = new Resident("Boberd", "Müller", "sdasdd 7", "Furtwangen", new Date(2002,06,4) );
        Resident resident6 = new Resident("Vladimir", "Herbert", "sdasdd 7", "Furtwangen", new Date(2002,06,4) );
        return Arrays.asList(resident1, resident2, resident3, resident4, resident5, resident6);
    }
}
