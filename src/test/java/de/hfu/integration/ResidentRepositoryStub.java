package de.hfu.integration;

import de.hfu.integration.domain.Resident;
import de.hfu.integration.repository.ResidentRepository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;



public class ResidentRepositoryStub implements ResidentRepository {


    private Resident resident1 = new Resident("Vadim", "Felker", "sdasdd 7", "Furtwangen", new Date(2000,02,23) );
    private Resident resident2 = new Resident("Serghei", "sadads", "sdasdd 7", "Furtwangen", new Date(2000,05,8) );
    private Resident resident3 = new Resident("Enis", "sadf", "sdasdd 7", "Furtwangen", new Date(2001,11,18) );
    private Resident resident4 = new Resident("Yannick", "Weber", "sdasdd 7", "Furtwangen", new Date(2003,01,27) );
    private Resident resident5 = new Resident("Boberd", "Müller", "sdasdd 7", "Furtwangen", new Date(2002,06,4) );
    private Resident resident6 = new Resident("Vladimir", "Herbert", "sdasdd 7", "Furtwangen", new Date(2002,06,4) );
    private List <Resident> residents;

    ResidentRepositoryStub() {
        residents = new ArrayList<>();
        residents.add(resident1);
        residents.add(resident2);
        residents.add(resident3);
        residents.add(resident4);
        residents.add(resident5);
        residents.add(resident6);
    }



    @Override
    public List<Resident> getResidents() {
        return null;
    }


}
