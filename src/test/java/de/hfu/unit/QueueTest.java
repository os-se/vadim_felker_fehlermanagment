package de.hfu.unit;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class QueueTest {


    Queue queue = new Queue(1);
    Queue queue1 = new Queue(2);

    @Test
    public void queueTest() {
        queue.enqueue(2);
        queue.enqueue(4);
        queue.enqueue(7);
        queue.enqueue(9);
        assertTrue(9==queue.dequeue());

        queue1.enqueue(20);
        queue1.enqueue(24);
        queue1.enqueue(29);
        queue1.enqueue(9);
        queue1.enqueue(14);
        int x = queue1.dequeue();
        assertTrue(9 == x,"dsd" + x);


    }
}
